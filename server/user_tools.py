import sqlite3
import asyncio
from datetime import datetime
from contextlib import closing
from hashlib import md5


class UserNotFound(Exception):
    pass

class UserAlreadyRegister(Exception):
    pass

class UserTools(object):

    USER_REGISTRATION_DATABASE = 'db.sqlite'

    def __init__(self, session_driver, dbfile=USER_REGISTRATION_DATABASE):
        self.__dbfile = dbfile
        self.__conn = sqlite3.connect(dbfile)
        self.__session_driver = session_driver
        self.__create_if_need()
        self.__lock = asyncio.Lock()

    async def get_username(self, session_id):
        return await self.__session_driver.get_username(session_id)

    async def get_users(self):
        return await self.__session_driver.get_users_list()

    async def remove_user(self, session_id=None, username=None):
        if session_id:
            await self.__session_driver.drop_session(session_id)
        elif not username:
            raise ValueError('Need username or session_id')
        await self.__session_driver.drop_user(username)

    def __make_password_hash(self, password):
        return md5(("%smysupersolt" % password).encode('utf-8')).hexdigest()

    async def __set_session_id(self, user, session_id):
        await self.__session_driver.set_session(
            user,
            session_id
        )

    async def reg_user(self, username, password):
        try:
            await self.__lock
            with closing(self.__conn.cursor()) as c:
                c.execute("select * from Users where username = ?", (username,))
                row = c.fetchone()
                if row:
                    raise UserAlreadyRegister()
                c.execute(
                    'insert into Users(username, password_hash) values (?, ?);',
                    (username, self.__make_password_hash(password))
                )
                self.__conn.commit()
        finally:
            self.__lock.release()

    async def auth_user(self, username, password):
        try:
            await self.__lock
            with closing(self.__conn.cursor()) as c:
                password_hash = self.__make_password_hash(password)
                c.execute("select username from Users where username = ? and password_hash = ?", (username, password_hash))
                row = c.fetchone()
                if not row:
                    raise UserNotFound()
                session_id = md5(("%ssupersoltagain" % datetime.utcnow().isoformat()).encode('utf-8')).hexdigest()
                await self.__set_session_id(username, session_id)
                return session_id
        finally:
            self.__lock.release()

    def __create_if_need(self):
        with closing(self.__conn.cursor()) as c:
            c.execute("create table if not exists Users (username varchar(128), password_hash varchar(64));")
