import asyncio
import asyncio_redis
from .base import BaseSessionDriver, SessionNotFound


import logging
logger = logging.getLogger(__name__)


class RedisSessionDriver(BaseSessionDriver):

    def __init__(self, rhost, rport, rdb):
        self.host, self.port, self.db_number = rhost, rport, rdb
        self.__is_init = False

    async def lazy_init(self):
        # Не хочу, чтобы два потока одновременно пытались инициализировать соединения
        l = asyncio.Lock()
        await l
        if self.__is_init:
            return
        # Здесь мы используем пул соединений, т.к. если использовать может случиться
        # так что мы пытаемся писать в редис, с тот момент, когда кто-то из него читает.
        # Пул соединений разруливает этот момент и дает нам первое свободное.
        self.conn = await asyncio_redis.Pool.create(host=self.host, port=self.port, db=self.db_number, poolsize=20)
        self.__is_init = True
        l.release()

    async def set_session(self, username, session_id):
        if not self.__is_init:
            await self.lazy_init()
        await self.conn.set("session"+session_id, username)
        await self.conn.set("user"+username, session_id)

    async def get_username(self, session_id):
        if not self.__is_init:
            await self.lazy_init()
        result =  await self.conn.get("session"+session_id)
        if result:
            return result
        raise SessionNotFound

    async def get_users_list(self):
        if not self.__is_init:
            await self.lazy_init()
        res = [username[4:] for username in (await (await self.conn.scan("user*")).fetchall())]
        return res

    async def get_sessions_by_username(self, username):
        if not self.__is_init:
            await self.lazy_init()
        return await self.conn.get('user'+username)

    async def drop_user(self, username):
        if not self.__is_init:
            await self.lazy_init()
        session_id = await self.get_sessions_by_username(username)
        await self.drop_session(session_id)

    async def drop_session(self, session_id):
        if not self.__is_init:
            await self.lazy_init()
        username = await self.conn.get("session"+session_id)
        await self.conn.delete(["session"+session_id])
        await self.conn.delete(["user"+username])
