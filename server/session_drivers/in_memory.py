from .base import BaseSessionDriver, SessionNotFound


class InMemorySessionDriver(BaseSessionDriver):

    def __init__(self):
        self.__sessions = {}

    async def set_session(self, username, session_id):
        self.__sessions[session_id] = username

    async def get_username(self, session_id):
        if not session_id in self.__sessions:
            raise SessionNotFound
        return self.__sessions[session_id]

    async def get_users_list(self):
        return list(self.__sessions.values())

    async def get_sessions_by_username(self, username):
        return [
            (session_id, un) for session_id, un in self.__sessions.items()
            if un == username
        ]

    async def drop_user(self, username):
        for session_id, _ in await self.get_sessions_by_username(username):
            await self.drop_session(session_id)

    async def drop_session(self, session_id):
        if session_id in self.__sessions:
            del self.__sessions[session_id]