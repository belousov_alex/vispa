import os
import asyncio
import asyncio_redis
import json
import websockets
import consts
import unittest

from server_engine import Server
from user_tools import UserTools
from session_drivers import InMemorySessionDriver, RedisSessionDriver
from redis_message_queue import RedisMessageQueue

def async_with_server(func):
    """
    "Подливает" тест в event loop с сервером.
    """
    def wrapped(*args, **kwargs):
        return asyncio.get_event_loop().run_until_complete(func(*args, **kwargs))
    return wrapped


class InMemoryServerTest(unittest.TestCase):

    PORT = 8888

    @property
    def curi(self):
        return 'ws://localhost:%d/ws' % self.PORT

    async def _registration(self, username, password, client):
        await client.send(json.dumps(dict(
            type=consts.REGISTRATION_REQUEST_MESSAGE,
            username=username,
            password=password
        )))
        return json.loads(await client.recv())

    async def _auth(self, username, password, client):
        await client.send(json.dumps(dict(
            type=consts.AUTH_REQUEST_MESSAGE,
            username=username,
            password=password
        )))
        return json.loads(await client.recv())

    async def _send_message(self, client, message, receiver=None, session_id=None):
        data = dict(
            message=message,
            type=consts.MESSAGE_TYPE_MESSAGE
        )
        if receiver:
            data['receiver'] = receiver
        if session_id:
            data['session_id'] = session_id
        await client.send(json.dumps(data))

    async def _user_list(self, client, session_id=None):
        data = dict(
            type=consts.USER_LIST_REQUEST_MESSAGE
        )
        if session_id:
            data['session_id'] = session_id
        await client.send(json.dumps(data))
        return json.loads(await client.recv())

    def _check_mess(self, data, message, is_success=True):
        self.assertEqual(
            data['type'],
            consts.MESSAGE_TYPE_MESSAGE if is_success else consts.ERROR_MESSAGE,
            'Пользователь должен получить сообщение'
        )
        if is_success:
            self.assertEqual(
                data['message'],
                message,
                'Пользователь должен получить сообщение %s' % message
            )


    @async_with_server
    async def test_registration(self):
        async with websockets.connect(self.curi) as client:

            result = await self._registration('TestRegistration', 'Password', client)
            self.assertEqual(
                result['type'],
                consts.REGISTRATION_ANSWER_MESSAGE,
                'Регистрация должна была пройти успешно'
            )

            result = await self._registration('TestRegistration', 'Password2', client)
            self.assertEqual(
                result['type'],
                consts.ERROR_MESSAGE,
                'Повторная регистрация, должна закончиться ошибкой.'
            )

    @async_with_server
    async def test_auth(self):
        async with websockets.connect('ws://localhost:8888') as client:

            result = await self._auth('NoFoundUser', 'Password', client)
            self.assertEqual(
                result['type'],
                consts.ERROR_MESSAGE,
                'Должна быть ошибка авторизации. Такой пользователь незарегистрирован.'
            )

            await self._registration('RTest', 'RPassword', client)

            result = await self._auth('RTest', 'FailPassword', client)
            self.assertEqual(
                result['type'],
                consts.ERROR_MESSAGE,
                'Не должно авторизовывать, если пароль не совпал, вместо этого: %s' % result
            )

            result = await self._auth('RTest', 'RPassword', client)
            self.assertEqual(
                result['type'],
                consts.AUTH_ANSWER_MESSAGE,
                'Должно авторизовать, вместо этого: %s' % result
            )

    @async_with_server
    async def test_noauth_user_message_deny(self):
        async with websockets.connect(self.curi) as client:
            await self._send_message(client, 'Hello!')
            data = json.loads(await client.recv())
            self.assertEqual(
                data['type'],
                consts.ERROR_MESSAGE,
                'Неавторизованные пользоватли, не могут писать сообщения.'
            )

    @async_with_server
    async def test_user_list(self):
        async with websockets.connect(self.curi) as client1:
            data = await self._user_list(client1)
            self.assertEqual(
                data['type'],
                consts.ERROR_MESSAGE,
                'Неавторизованные пользоватли не могут смотреть список.'
            )

            await self._registration('ULTest1', 'Pass', client1)
            session_id = (await self._auth('ULTest1', 'Pass', client1))['session_id']

            data = await self._user_list(client1, session_id)
            self.assertEqual(
                data['type'],
                consts.USER_LIST_ANSWER_MESSAGE,
                'Авторизованный пользователь может смотреть список. %s' % data
            )

            self.assertEqual(
                data['users'],
                ['ULTest1'],
                'Список пользователей должен состоять из одного пользователя. %s' % data
            )

            # Подключаем второго пользователя.
            async with websockets.connect(self.curi) as client2:
                await self._registration('ULTest2', 'Pass', client2)
                session_id2 = (await self._auth('ULTest2', 'Pass', client2))['session_id']

                data = await self._user_list(client2, session_id2)
                self.assertEqual(
                    sorted(data['users']),
                    sorted(['ULTest1', 'ULTest2']),
                    'Список пользователей должен состоять из двух пользователей. %s' % data
                )

            # в случае RedisBasedServer-а  нужно немного подождать,
            # чтобы он успел сходить в redis и удалить пользователя
            await asyncio.sleep(0.1)

            data = await self._user_list(client1, session_id)
            self.assertEqual(
                data['users'],
                ['ULTest1'],
                'Список пользователей должен состоять из одного пользователя. %s' % data
            )

    @async_with_server
    async def test_message(self):
        async with websockets.connect(self.curi) as client1:
            async with websockets.connect(self.curi) as client2:
                async with websockets.connect(self.curi) as client3:
                    await self._registration('U1', 'p', client1)
                    session1 = (await self._auth('U1', 'p', client1))['session_id']
                    await self._registration('U2', 'p', client2)
                    session2 = (await self._auth('U2', 'p', client2))['session_id']
                    await self._registration('U3', 'p', client3)
                    session3 = (await self._auth('U3', 'p', client3))['session_id']

                    # Три пользователя получают сообщение
                    await self._send_message(client1, 'Hello!', session_id=session1)
                    crtn1 = asyncio.ensure_future(client1.recv())
                    crtn2 = asyncio.ensure_future(client2.recv())
                    crtn3 = asyncio.ensure_future(client3.recv())
                    await asyncio.wait([crtn1, crtn2, crtn3], timeout=0.2)
                    data1 = json.loads(crtn1.result())
                    data2 = json.loads(crtn2.result())
                    data3 = json.loads(crtn3.result())

                    self._check_mess(data1, 'Hello!')
                    self._check_mess(data2, 'Hello!')
                    self._check_mess(data3, 'Hello!')

                    # Первый пользователь не получает сообщения
                    await self._send_message(client3, 'Hello, U2!', receiver='U2', session_id=session3)
                    crtn1 = asyncio.ensure_future(client1.recv())
                    crtn2 = asyncio.ensure_future(client2.recv())
                    crtn3 = asyncio.ensure_future(client3.recv())
                    await asyncio.wait([crtn1, crtn2, crtn3], timeout=0.2)
                    # ожидаем что ругнется на отсутсвие ответа
                    with self.assertRaises(asyncio.futures.InvalidStateError):
                        json.loads(crtn1.result())
                    crtn1.cancel() # отменяем задачу во избежание сайд эффектов
                    data2 = json.loads(crtn2.result())
                    data3 = json.loads(crtn3.result())

                    self._check_mess(data2, 'Hello, U2!')
                    self._check_mess(data3, 'Hello, U2!')


class RedisBasedServerTest(InMemoryServerTest):

    PORT = 8881
    PORT2 = 8882

    @property
    def curi2(self):
        return 'ws://localhost:%d/ws' % self.PORT2

    @async_with_server
    async def test_multiserver_messaging(self):

        async with websockets.connect(self.curi) as client1:
            async with websockets.connect(self.curi2) as client2:
                async with websockets.connect(self.curi) as client3:
                    await self._registration('U1', 'p', client1)
                    session1 = (await self._auth('U1', 'p', client1))['session_id']
                    await self._registration('U2', 'p', client2)
                    session2 = (await self._auth('U2', 'p', client2))['session_id']
                    await self._registration('U3', 'p', client3)
                    session3 = (await self._auth('U3', 'p', client3))['session_id']

                    # Три пользователя получают сообщение
                    await self._send_message(client1, 'Hello!', session_id=session1)
                    crtn1 = asyncio.ensure_future(client1.recv())
                    crtn2 = asyncio.ensure_future(client2.recv())
                    crtn3 = asyncio.ensure_future(client3.recv())
                    await asyncio.wait([crtn1, crtn2, crtn3], timeout=0.2)
                    data1 = json.loads(crtn1.result())
                    data2 = json.loads(crtn2.result())
                    data3 = json.loads(crtn3.result())

                    self._check_mess(data1, 'Hello!')
                    self._check_mess(data2, 'Hello!')
                    self._check_mess(data3, 'Hello!')

                    # Первый пользователь не получает сообщения
                    await self._send_message(client3, 'Hello, U2!', receiver='U2', session_id=session3)
                    crtn1 = asyncio.ensure_future(client1.recv())
                    crtn2 = asyncio.ensure_future(client2.recv())
                    crtn3 = asyncio.ensure_future(client3.recv())
                    await asyncio.wait([crtn1, crtn2, crtn3], timeout=0.2)
                    # ожидаем что ругнется на отсутсвие ответа
                    with self.assertRaises(asyncio.futures.InvalidStateError):
                        json.loads(crtn1.result())
                    crtn1.cancel() # отменяем задачу во избежание сайд эффектов
                    data2 = json.loads(crtn2.result())
                    data3 = json.loads(crtn3.result())

                    self._check_mess(data2, 'Hello, U2!')
                    self._check_mess(data3, 'Hello, U2!')

    @async_with_server
    async def test_user_list_multiserver(self):
        async with websockets.connect(self.curi) as client1:
            data = await self._user_list(client1)
            self.assertEqual(
                data['type'],
                consts.ERROR_MESSAGE,
                'Неавторизованные пользоватли не могут смотреть список.'
            )

            await self._registration('ULTest1', 'Pass', client1)
            session_id = (await self._auth('ULTest1', 'Pass', client1))['session_id']

            data = await self._user_list(client1, session_id)
            self.assertEqual(
                data['type'],
                consts.USER_LIST_ANSWER_MESSAGE,
                'Авторизованный пользователь может смотреть список. %s' % data
            )

            self.assertEqual(
                data['users'],
                ['ULTest1'],
                'Список пользователей должен состоять из одного пользователя. %s' % data
            )

            # Подключаем второго пользователя.
            async with websockets.connect(self.curi2) as client2:
                await self._registration('ULTest2', 'Pass', client2)
                session_id2 = (await self._auth('ULTest2', 'Pass', client2))['session_id']

                data = await self._user_list(client2, session_id2)
                self.assertEqual(
                    sorted(data['users']),
                    sorted(['ULTest1', 'ULTest2']),
                    'Список пользователей должен состоять из двух пользователей. %s' % data
                )

            # в случае RedisBasedServer-а  нужно немного подождать,
            # чтобы он успел сходить в redis и удалить пользователя
            await asyncio.sleep(0.1)

            data = await self._user_list(client1, session_id)
            self.assertEqual(
                data['users'],
                ['ULTest1'],
                'Список пользователей должен состоять из одного пользователя. %s' % data
            )



async def drop_all_session(rhost, rport, rdb):
    """
    Удаляем все записи из тестовой БД
    """
    conn = await asyncio_redis.Connection.create(host=rhost, port=rport, db=rdb)
    all_keys = await (await conn.scan("*")).fetchall()
    if all_keys:
        await conn.delete(all_keys)

if __name__ == '__main__':
    # Удаляем тестовую sqlite базу с данными пользователя.
    if os.path.exists('test.sqlite'):
        os.remove('test.sqlite')
    if os.path.exists('test2.sqlite'):
        os.remove('test2.sqlite')
    asyncio.get_event_loop().run_until_complete(drop_all_session('localhost', 6379, 2))

    # Поднимаем inmemory сервер в event loop, чтобы к нему можно было обращаться из тестов.
    Server(
        'localhost', InMemoryServerTest.PORT,
        UserTools(InMemorySessionDriver(), 'test.sqlite'),
        asyncio.Queue()
    ).start(asyncio.get_event_loop())

    # Поднимаем два сервера с очередь на redis
    Server(
        'localhost', RedisBasedServerTest.PORT,
        UserTools(RedisSessionDriver('localhost', 6379, 2), 'test2.sqlite'),
        RedisMessageQueue('localhost', 6379, 2)
    ).start(asyncio.get_event_loop())
    Server(
        'localhost', RedisBasedServerTest.PORT2,
        UserTools(RedisSessionDriver('localhost', 6379, 2), 'test2.sqlite'),
        RedisMessageQueue('localhost', 6379, 2)
    ).start(asyncio.get_event_loop())

    unittest.main()