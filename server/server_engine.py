import asyncio
import json
from functools import partial

import websockets
from websockets.exceptions import ConnectionClosed

import consts
from session_drivers import SessionNotFound
from user_tools import UserNotFound, UserAlreadyRegister

import logging
logger = logging.getLogger(__name__)


class UnauthorizedUser(Exception):
    """
    Выбрасываем, когда видим что сессии нет или же она не активна.
    """
    pass


def need_auth(func):
    """
    Не очень красиво сделал. Хотел декоратор,
    который бы проверял авторизован ли пользователь или нет.
    Приходится залезать в экземпляр ApiHandlers за объектом UserTools
    """
    async def wrapped(*args, **kwargs):
        api = kwargs.get('self') or args[0]
        data = kwargs.get('data') or args[1]
        if 'session_id' not in data:
            raise UnauthorizedUser()
        try:
            username = await api.user_tools.get_username(data['session_id'])
        except SessionNotFound:
            raise UnauthorizedUser()
        return await func(*args, username=username, **kwargs)

    return wrapped

class ApiHandlers(object):

    def __init__(self, server, user_tools):
        self.__server = server
        self.user_tools = user_tools

    async def auth(self, data, websocket):
        try:
            session_id = await self.user_tools.auth_user(data['username'], data['password'])
            logger.info('Login %s, session_id %s' % (data['username'], session_id))
            self.__server.add_user(data['username'], websocket)
        except UserNotFound:
            logger.info('User with username=%s and password=%s not found.' % (data['username'], data['password']))
            return dict(error='User not found.', type=consts.ERROR_MESSAGE)
        return dict(session_id=session_id, type=consts.AUTH_ANSWER_MESSAGE)

    async def registration(self, data):
        try:
            await self.user_tools.reg_user(data['username'], data['password'])
            logger.info('Registration %s' % data['username'])
        except UserAlreadyRegister:
            logger.info('Username %s already register' % data['username'])
            return dict(error='User already register.', type=consts.ERROR_MESSAGE)
        return dict(type=consts.REGISTRATION_ANSWER_MESSAGE)

    @need_auth
    async def message(self, data, message_bus, username):
        data['sender'] = username
        await message_bus.put(data)

    @need_auth
    async def users_list(self, data, username):
        return dict(users=await self.user_tools.get_users(), type=consts.USER_LIST_ANSWER_MESSAGE)


class Server(object):

    def __init__(self, host, port, user_tools, message_bus):
        if isinstance(message_bus, asyncio.Queue):
            logging.info('Use inmemory version server.')
        else:
            logging.info('Use redis pubsub version server.')
        self.__host, self.__port = host, port
        # Здесь все подключенные к этому серверу пользователи с авторзацией
        self.__connected_users = {}
        # Моделька для работы с пользователями
        self.__user_tools = user_tools
        # Класс, реализующий обработчики запросов
        self.__api = ApiHandlers(self, self.__user_tools)
        self.__message_bus = message_bus

    def start(self, loop):
        # одина процедура обрабатывает сообщения из очереди, вторая - запросы от пользователя
        asyncio.get_event_loop().create_task(self.handle_messages())
        asyncio.get_event_loop().run_until_complete(
            websockets.serve(self.handle_requests, self.__host, self.__port)
        )

    def __make_router(self, websocket):
        return {
            consts.AUTH_REQUEST_MESSAGE: partial(self.__api.auth, websocket=websocket),
            consts.REGISTRATION_REQUEST_MESSAGE: self.__api.registration,
            consts.MESSAGE_TYPE_MESSAGE: partial(self.__api.message, message_bus=self.__message_bus),
            consts.USER_LIST_REQUEST_MESSAGE: self.__api.users_list,
        }

    async def handle_messages(self):
        while True:
            mess_data = await self.__message_bus.get()
            logger.debug('Receive message from queue: %s' % mess_data)
            sendings = []
            for username, s in self.connected_users:
                if (not mess_data.get('receiver')
                    or username == mess_data['sender']
                    or username == mess_data.get('receiver')
                ):
                    sendings.append(asyncio.ensure_future(s.send(json.dumps(mess_data))))
                    logger.debug('Sent data to username=%s: %s' % (username, mess_data))
            if sendings:
                await asyncio.wait(sendings)

    async def handle_requests(self, websocket, path):
        router = self.__make_router(websocket)
        logger.info('New connection')
        while True:
            try:
                raw_data = await websocket.recv()
                logger.debug('Receive data: %s' % raw_data)
                data = json.loads(raw_data)
                # смотрим тип сообщения
                if 'type' not in data or data['type'] not in router:
                    result = dict(error='Unknown type message', type=consts.ERROR_MESSAGE)
                    await websocket.send(json.dumps(result))
                    logger.debug('Sent data: %s' % json.dumps(result))
                    continue

                # дергаем обработчик
                try:
                    result = await router[data['type']](data)
                except UnauthorizedUser:
                    result = dict(type=consts.ERROR_MESSAGE, error='Please log in.')
                    await websocket.send(json.dumps(result))
                    logger.debug('Sent data: %s' % json.dumps(result))
                    continue

                # Если это сценарий "запрос-ответ", то нужно вернуть ответ пользователю
                if result:
                    await websocket.send(json.dumps(result))
                    logger.debug('Sent data: %s' % json.dumps(result))
            except ConnectionClosed:
                await self.remove_user(websocket=websocket)
                logger.info('Close connection')
                break

    def add_user(self, username, websocket):
        """
        Добавляем авторизованного пользователя в словарь, чтобы потом можно было рассылать ему сообщения.
        :param username: имя пользователя
        :param websocket: сокет
        """
        self.__connected_users[username] = websocket

    async def remove_user(self, username=None, websocket=None):
        """
        Удаляем пользователя из списка по имени или по сокету.
        :param username: имя
        :param websocket: сокет
        """
        # Выбираем по пользователя по его сокету
        if websocket:
            k = list(filter(lambda p: p[1] == websocket, self.__connected_users.items()))
            username = None if not k else k[0][0]
        elif not username:
            raise ValueError('Need username or websocket object.')
        if username:
            del self.__connected_users[username]
            await self.__user_tools.remove_user(username=username)

    async def cleanup(self):
        '''
        Удаляем все сессии подлюченных пользователей.
        :return:
        '''
        for username in self.__connected_users.keys():
            await self.remove_user(username)

    @property
    def connected_users(self):
        return self.__connected_users.items()

    def __del__(self):
        """
        Подчищаем.
        """
        asyncio.get_event_loop().run_until_complete(self.cleanup())
