from argparse import ArgumentParser
from runner import run_server

import logging
logger = logging.getLogger('')


def parse_args():
    p = ArgumentParser()
    p.add_argument('--port', default=8765, type=int, help='Порт сервера')
    p.add_argument('--host', default='localhost',  help='Адрес сервера')
    p.add_argument('--in-memory', action='store_true',
        help='Использовать inmemory версию сервера, в качестве источника сообщений будет использоваться asyncio.Queue, '
             'а не Redis pubsub.'
    )
    p.add_argument('--r-host', default='localhost', help='Адрес redis сервера')
    p.add_argument('--r-port', default=6379, type=int, help='Порт redis сервера')
    p.add_argument('--r-db', default=10, type=int, help='Номер базы redis')
    return p.parse_args()

if __name__ == "__main__":
    logging.basicConfig()
    logger.setLevel(logging.DEBUG)
    logging.getLogger('websockets').setLevel(logging.ERROR)
    logging.getLogger('asyncio').setLevel(logging.ERROR)
    logging.getLogger('asyncio_redis').setLevel(logging.ERROR)
    args = parse_args()
    run_server(args.host, args.port, None if args.in_memory else (args.r_host, args.r_port, args.r_db))