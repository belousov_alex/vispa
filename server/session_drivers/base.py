from abc import ABCMeta, abstractmethod

class SessionNotFound(Exception):
    pass

class BaseSessionDriver(metaclass=ABCMeta):

    @abstractmethod
    async def set_session(self, username, session_id):
        raise NotImplementedError

    @abstractmethod
    async def get_username(self, session_id):
        raise NotImplementedError

    @abstractmethod
    async def get_users_list(self):
        raise NotImplementedError

    @abstractmethod
    async def drop_session(self, session_id):
        raise NotImplementedError

    @abstractmethod
    async def get_sessions_by_username(self, username):
        raise NotImplementedError

    @abstractmethod
    async def drop_session(self, session_id):
        raise NotImplementedError
