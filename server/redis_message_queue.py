import json
import asyncio
import asyncio_redis

import logging
logger = logging.getLogger(__name__)


class RedisMessageQueue(object):
    """
    Очередь-обертка над механизмом pubsub redis.
    """

    def __init__(self, host, port, db_number):
        self.host, self.port, self.db_number = host, port, db_number
        self.__is_init = False

    async def lazy_init(self):
        # Не хочу, чтобы два потока одновременно пытались инициализировать соединения
        l = asyncio.Lock()
        await l
        if self.__is_init:
            return
        # redis не позволяет публиковать сообщения, в тот момент пока мы ждем опубликованных сообщений,
        # поэтому мы делаем два соединения, одно для публикаций, второе для подписки
        self.conn_for_publish = await asyncio_redis.Connection.create(host=self.host, port=self.port, db=self.db_number)
        self.conn_for_subscribe = await asyncio_redis.Connection.create(host=self.host, port=self.port, db=self.db_number)
        self.conn_for_subscribe.subscriber = await self.conn_for_subscribe.start_subscribe()
        await self.conn_for_subscribe.subscriber.subscribe(['messages'])
        self.__is_init = True
        l.release()

    async def put(self, message_data):
        if not self.__is_init:
            await self.lazy_init()
        await self.conn_for_publish.publish('messages', json.dumps(message_data))

    async def get(self):
        if not self.__is_init:
            await self.lazy_init()
        mess = await self.conn_for_subscribe.subscriber.next_published()
        return json.loads(mess.value)
