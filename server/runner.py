import asyncio

from server_engine import Server
from user_tools import UserTools
from session_drivers import RedisSessionDriver
from redis_message_queue import RedisMessageQueue

import logging
logger = logging.getLogger(__name__)


def run_server(host, port, redis_config):
    if redis_config:
        queue = RedisMessageQueue(*redis_config)
    else:
        queue = asyncio.Queue()
    s = Server(host, port, UserTools(RedisSessionDriver(*redis_config), 'db.sqlite'), queue)
    s.start(asyncio.get_event_loop())
    logger.info('Start server on %s and %d port.' % (host, port))
    try:
        asyncio.get_event_loop().run_forever()
    except:
        asyncio.get_event_loop().run_until_complete(s.cleanup())